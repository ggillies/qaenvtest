local environments = std.split(std.extVar('environments'), ',');

local stages = ['diff', 'deploy'];

local diff(environment) = {
  image: 'bash:latest',
  stage: 'diff',
  script: [
    'apk add --no-cache go jq git curl',
    'git clone --depth 1 https://github.com/asdf-vm/asdf.git ${HOME}/.asdf',
    '. ${HOME}/.asdf/asdf.sh',
    "for i in $(cut -f1 -d ' ' .tool-versions);do asdf plugin add $i;done",
    'asdf install',
    'helmfile -e ' + environment + ' template',
  ],
  rules: [
    {
      'if': '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH',
      changes:
        {
          paths: [
            'environments/' + environment + '/*',
            'global-values.yaml',
            'helmfile.yaml',
          ],
          compare_to: 'main',
        },
    },
    {
      'if': '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
      changes:
        {
          paths: [
            'environments/' + environment + '/*',
            'global-values.yaml',
            'helmfile.yaml',
          ],
        },
    },
  ],
  environment: environment,
};
local deploy(environment) = {
  image: 'bash:latest',
  stage: 'deploy',
  script: [
    'apk add --no-cache go jq git curl',
    'git clone --depth 1 https://github.com/asdf-vm/asdf.git ${HOME}/.asdf',
    '. ${HOME}/.asdf/asdf.sh',
    "for i in $(cut -f1 -d ' ' .tool-versions);do asdf plugin add $i;done",
    'asdf install',
    'helmfile -e ' + environment + ' template',
  ],
  rules: [
    {
      'if': '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
      changes:
        {
          paths: [
            'environments/' + environment + '/*',
            'global-values.yaml',
            'helmfile.yaml',
          ],
        },
    },
  ],
  environment: environment,
};

local diffs = {
  ['%s:diff' % environment]: diff(environment)
  for environment in environments
};

local deploys = {
  ['%s:deploy' % environment]: deploy(environment)
  for environment in environments
};

{'stages': stages} + diffs + deploys
